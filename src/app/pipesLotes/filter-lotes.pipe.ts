import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterLotes'
})
export class FilterLotesPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    if(arg == '') return value;
    const resultadopost = [];
    for(const post of value){
      if(post.nombre.toLowerCase().indexOf(arg.toLowerCase()) > -1){
        resultadopost.push(post)
      }
      else if(post.uso_suelo.toLowerCase().indexOf(arg.toLowerCase()) > -1){
        resultadopost.push(post)
      }
    }
    return resultadopost;
  }

}
