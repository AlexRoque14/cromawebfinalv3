import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { AnalisisComponent } from './components/analisis/analisis.component';
import { AdminComponent } from './components/admin/admin.component';
import { RegistroLoteComponent } from './components/registro-lote/registro-lote.component';
import { ListaLotesComponent } from './components/lista-lotes/lista-lotes.component';
import { PerfilComponent } from './components/perfil/perfil.component';
import {ListaCromasComponent} from './components/lista-cromas/lista-cromas.component';
import { ActivateAccountComponent } from './components/activate-account/activate-account.component'
import { MapaComponent } from './components/mapa/mapa.component';
import { AllCromannComponent } from './components/all-cromann/all-cromann.component';
import { FPasswordComponent } from './components/f-password/f-password.component';
import { LiderUserComponent } from './components/lider-user/lider-user.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },{
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'analisis',
    component: AnalisisComponent
  },
  {
    path: 'admin',
    component: AdminComponent
  },
  {
    path: 'registro-lote',
    component: RegistroLoteComponent
  },
  {
    path: 'all-cromann',
    component: AllCromannComponent
  },
  {
    path: 'lista-lotes',
    component: ListaLotesComponent
  },
  {
    path: 'lista-cromas',
    component: ListaCromasComponent
  },
  {
    path: 'perfil',
    component: PerfilComponent
  },
  {
    path: 'activate/:token',
    component: ActivateAccountComponent
  },
  {
    path: 'mapa',
    component: MapaComponent
  },
  {
    path: 'fpassword/:token',
    component: FPasswordComponent
  },
  {
    path: 'user&lider',
    component: LiderUserComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
