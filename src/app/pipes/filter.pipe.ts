import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    if(arg == '') return value;
    const resultadopost = [];
    for(const post of value){
      if(post.email.toLowerCase().indexOf(arg.toLowerCase()) > -1){
        resultadopost.push(post)
      }
    }
    return resultadopost;
  }

}
