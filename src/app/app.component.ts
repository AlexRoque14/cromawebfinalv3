import { Component } from '@angular/core';
import { OnInit, ViewChild, ElementRef, Input } from '@angular/core';  

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'CromaV4';
}
