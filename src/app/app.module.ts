import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { AnalisisComponent } from './components/analisis/analisis.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { AdminComponent } from './components/admin/admin.component';

//Agregados
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import {FormsModule} from '@angular/forms';
import { RegistroLoteComponent } from './components/registro-lote/registro-lote.component';
import { ListaLotesComponent } from './components/lista-lotes/lista-lotes.component';
import { PerfilComponent } from './components/perfil/perfil.component';
import { AgmCoreModule } from '@agm/core';
import { ListaCromasComponent } from './components/lista-cromas/lista-cromas.component';
import { ActivateAccountComponent } from './components/activate-account/activate-account.component';
import { MapaComponent } from './components/mapa/mapa.component';
import { AllCromannComponent } from './components/all-cromann/all-cromann.component';
import { FPasswordComponent } from './components/f-password/f-password.component';
import { LiderUserComponent } from './components/lider-user/lider-user.component';
import { FilterPipe } from './pipes/filter.pipe';
import { FooterComponent } from './components/footer/footer.component';
import { FilterLotesPipe } from './pipesLotes/filter-lotes.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    AnalisisComponent,
    NavbarComponent,
    AdminComponent,
    RegistroLoteComponent,
    ListaLotesComponent,
    PerfilComponent,
    ListaCromasComponent,
    ActivateAccountComponent,
    MapaComponent,
    AllCromannComponent,
    FPasswordComponent,
    LiderUserComponent,
    FilterPipe,
    FooterComponent,
    FilterLotesPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCDv2DGOYWOXelfoCqJpNXRR2DoMJUst-0',
      libraries: ['places']
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
