import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class DataApiService {
  
  constructor(private http: HttpClient) { }

  user: Observable<any>;
  users: Observable<any>;
  IA: string = "https://ia.v2.cromasesiia.com/upload/"
  URL2: string = "https://cromasesiia.com/api/v2/";
  URL: string = "https://morning-stream-44468.herokuapp.com/api/v2";

  headers: HttpHeaders = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  // IA-------------------------------
  IAReturn(body:any){
    return this.http.post(this.IA, body);
  }

  //REGISTRO DE USUARIO----------------
  /* registro de usuario */
  registroUsuario(body: any): Observable<any> {
    return this.http.post(this.URL + '/users', body);
  }
  //------------------------------------------

  //-------INICIO DE SESIÓN ------------------
  /* login de usuario */
  login(body: any): Observable<any> {
    return this.http.post(this.URL + '/login', body);
  }
  //-----------------------------------------

  //------- USUARIOS -------------------------
  /* todos los usuarios */
  getAllUsers() {
    return this.http.get(this.URL + '/users/todos');
  }

  /* user por id */
  getUserId(id: String) {
    return this.http.get(this.URL + '/users/' + id);
  }

  //-------------------------------------------

  //---------ACTIVACION Y DESACTIVACION DE USUARIOS --------
  /* Activar usuarios */
  ActivarUsuario(id: String, body: any): Observable<any> {
    return this.http.put(this.URL + '/users/' + id, body);
  }

  /* Desactivar usuarios */
  DesactivarUsuario(id: String, body: any): Observable<any> {
    return this.http.put(this.URL + '/users/' + id, body);
  }

  /* Confirmación de cuenta por correo */
  ConfCuenta(token: String, body: any): Observable<any> {
    return this.http.put(this.URL + '/actemail/' + token, body);
  }

  /* trae usuarios por inactivos*/
  getAllUsersIn() {
    return this.http.get(this.URL + '/inactivos');
  }

  /* trae usuarios por inactivos*/
  getAllUsersAc() {
    return this.http.get(this.URL + '/activos');
  }
  getAllUsersLi() {
    return this.http.get(this.URL + '/lideres');
  }
  getAllUsersSe(id:string) {
    return this.http.get(this.URL + '/seguidores/' + id);
  }
  // Busca usuario por email
  getUserEmail(email:string, body:any) {
    return this.http.put(this.URL + '/email/'+ email, body);
  }
  //-------------------------------------------

  //--------- PAISES ---------------------------
  /* todos los paises */
  getPais() {
    return this.http.get(this.URL + '/paises');
  }

  getEstados(id: String) {
    return this.http.get(this.URL + '/paises/' + id);
  }

  getMunicipios(id: String) {
    return this.http.get(this.URL + '/estados/' + id);
  }

  //---------------REGISTRO DE LOTES--------------
  /* creacion de lote*/
  registroLote(body: any) {
    return this.http.post(this.URL + '/lotes', body);
  }

  LoteById(id: String) {
    return this.http.get(this.URL + '/lotes/' + id);
  }
  /* Eliminar Lotes */

  deleteLote(id: String, body: any) {
    return this.http.put(this.URL + '/lotes/' + id, body)
  }

  /*lotes por id del usuario*/
  lotesByIdUser(id: String) {
    return this.http.get(this.URL + '/user/lotes/' + id);
  }
  //----------------------------------------------

  //-------------CORREO ELECTRONICO----------------
  /* notificacion por correo electronico para activacion*/
  sendEmail(body: any) {
    return this.http.post(this.URL2 + 'email', body)
  }
  sendEmailPassword(body: any) {
    return this.http.post(this.URL2 + 'password', body)
  }
//-------------Subir IMg----------------
  sendIMG(body: any) {
    return this.http.post(this.URL2 + 'img', body)
  }

  //------------------MUESTRA CROMA--------------------
  AllMuestras(user_id: String){
    return this.http.get( this.URL + '/lote/muestras/' + user_id)
  }

  addCroma(body:any){
    return this.http.post( this.URL + '/cromann', body)
  }

  CromaById(id:String){
    return this.http.get( this.URL + '/cromann/' + id)
  }

  AllCromas(){
    return this.http.get( this.URL + '/cromann/')
  }

  EditCroma(id:String, body:any){
    return this.http.put( this.URL + '/cromann/' + id, body)
  }

  MuestrasMapa(){
    return this.http.get( this.URL + '/todas/muestras')
  }
  

}
