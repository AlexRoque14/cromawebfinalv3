import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiderUserComponent } from './lider-user.component';

describe('LiderUserComponent', () => {
  let component: LiderUserComponent;
  let fixture: ComponentFixture<LiderUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiderUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiderUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
