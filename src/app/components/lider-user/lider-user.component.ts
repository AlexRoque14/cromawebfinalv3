import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder , Validators } from '@angular/forms';
import { DataApiService } from '../../services/data-api.service'
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-lider-user',
  templateUrl: './lider-user.component.html',
  styleUrls: ['./lider-user.component.css']
})
export class LiderUserComponent implements OnInit {

  constructor(private API: DataApiService, 
    public FB: FormBuilder, 
    private route: Router) { }

  Lideres : any
  filterPost = ''

  public rol =  localStorage.getItem('rol_user');
  siguiendo : any

  seguir:FormGroup
  dejarseguir:FormGroup

  seguidores:any
  ngOnInit() {
    this.pagValid()
    this.getLideres()
  }

  async getLideres(){
    this.dejarseguir = this.FB.group({
      user_id:['']
    })
    if(this.rol == '4'){
      this.seguir = this.FB.group({
        user_id:['']
      })
      await this.API.getUserId(localStorage.getItem('id_user')).subscribe(async response =>{
        this.siguiendo = response['user']['user_id']
      })
      this.API.getAllUsersLi().subscribe(response=>{
        this.Lideres = response
      })
    }else if (this.rol == '3'){
      this.API.getAllUsersSe(localStorage.getItem('id_user')).subscribe(response=>{
        this.Lideres = response
        this.seguidores = this.Lideres.length
      })
    }
  }

  Seguir(id:string, follow:any){
    follow.user_id=id
    console.log(follow);
    if(!this.siguiendo){
      this.API.ActivarUsuario(localStorage.getItem('id_user'), follow).subscribe(response=>{
        Swal.fire("¡Buen trabajo!", "Acabas de seguir a un usuario lider.", "success");
        this.ngOnInit() 
      })
    }else{
      this.API.ActivarUsuario(localStorage.getItem('id_user'), follow).subscribe(response=>{
        Swal.fire("¡Advertencia!", "Acabas de dejar de seguir a un usuario lider para seguir a otro usuario lider.", "info");
        this.ngOnInit() 
      })
    }
  }
  DejardeSeguir(id:String, unfollow:any){
    console.log(unfollow);
    this.API.ActivarUsuario(localStorage.getItem('id_user'), unfollow).subscribe(response=>{
      Swal.fire("¡Advertencia!", "Acabas de dejar de seguir a un usuario lider.", "info");
      this.ngOnInit()    
    })
  }
  Eliminar(id:String, unfollow:any){
    console.log(unfollow);
    this.API.ActivarUsuario(id, unfollow).subscribe(response=>{
      Swal.fire("¡Advertencia!", "Acabas de remover a un seguidor tuyo.", "info");
      this.ngOnInit()    
    })
  }

  pagValid(){
    if(this.rol == '0' || this.rol != '4' && this.rol != '3'){
      this.route.navigate(['']);
    }
  }

}
