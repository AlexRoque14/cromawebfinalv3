import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder , Validators } from '@angular/forms';
import { DataApiService } from '../../services/data-api.service'
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { print } from 'util';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registro: FormGroup;
  registroL: FormGroup;
  sendEmail: FormGroup;
  Paises: any;
  Estados: any;
  Municipios: any;

  Pais: "País";
  Estado: String;
  Municipio: String;
  MunId: String;

  constructor(private API: DataApiService, 
    public FB: FormBuilder, 
    private route: Router) { }

  async ngOnInit() {
    this.Pais = "País"
    this.Estado = "Estado"
    this.Municipio = "Municipio"
    await this.pais()
    this.sendEmail = this.FB.group({
      email:[''],
      nombre:[''],
      token:['']
    })
    this.registro = this.FB.group({
      nombre:['', Validators.required],
      ape_p:['', Validators.required],
      ape_m:['', Validators.required],
      email:['', Validators.required],
      password:['', Validators.required],
      municipio_id:['', Validators.required],
      direccion:['', Validators.required],
      telefono:['', Validators.required],
      rol:['4', Validators.required],
    })
    this.registroL = this.FB.group({
      nombre:['', Validators.required],
      ape_p:['', Validators.required],
      ape_m:['', Validators.required],
      email:['', Validators.required],
      password:['', Validators.required],
      municipio_id:['', Validators.required],
      direccion:['', Validators.required],
      telefono:['', Validators.required],
      rol:['3', Validators.required],
    })
  }

  register(formregistro:any, SendEmail: any){
    if (formregistro.nombre == "" || formregistro.ape_pat == ""  || formregistro.ape_mat == "" 
        || formregistro.email == "" || formregistro.password == "" || this.MunId == "" 
        || formregistro.direccion == "" || formregistro.telefono == "" || formregistro.rol == ""){
      Swal.fire("Error", "Complete los datos faltantes", "error");
    }else{
      formregistro.municipio_id = this.MunId
      this.API.registroUsuario(formregistro).subscribe(response=>{        

        SendEmail.token = response['token_activacion']
        SendEmail.email = response['email']
        SendEmail.nombre = response['nombre']
        
        this.API.sendEmail(SendEmail).subscribe(response=>{})
        Swal.fire("¡Buen trabajo!", "Registro Exitoso. Revisa tu correo para activar tu cuenta.", "success");
        this.ngOnInit();
        this.route.navigate(['login']);
      })

    }
  }
  async pais(){
    await this.API.getPais().subscribe(async response =>{
      this.Paises = await response;
      console.log(this.Paises)
    })
  }
  extraeidPais(id, pais){
    this.Pais=pais
    this.API.getEstados(id).subscribe(response =>{
      this.Estados = response;
    })
  }
  extraeidEstado(id, estado){
    this.Estado = estado
    this.API.getMunicipios(id).subscribe(response =>{
      this.Municipios = response['estadomunicipio'];
    })
  }

  extraeidMuni(id, muni){
    this.MunId = id
    this.Municipio = muni
  }
  
}
