import { Component, OnInit } from '@angular/core';
import { DataApiService } from 'src/app/services/data-api.service';
import { Router } from '@angular/router';
import { NumberSymbol } from '@angular/common';
import { FormGroup, FormBuilder } from '@angular/forms';
import Swal from 'sweetalert2';
import { MapsAPILoader } from '@agm/core';

@Component({
  selector: 'app-lista-lotes',
  templateUrl: './lista-lotes.component.html',
  styleUrls: ['./lista-lotes.component.css']
})
export class ListaLotesComponent implements OnInit {
  lat: number;
  lng: number;
  desactivar : FormGroup;
  zoom: number;
  private geoCoder;
  address: string;
  viewType: any='hybrid';

  filterLotePost = ''

  public rol =  localStorage.getItem('rol_user');
  constructor(private API: DataApiService,  public FB: FormBuilder, private route: Router,    private mapsAPILoader: MapsAPILoader,) { }
  
  lotes:any;
  lot: any;
  public Tamano : any;
  
  ngOnInit() {
    this.pagValid();
    this.desactivar = this.FB.group({
      eliminado: ["true"]
    });
    this.mapsAPILoader.load().then(() => {
      this.geoCoder = new google.maps.Geocoder;
    });
    
    this.pagValid();
    this.getLotesByIdUser();
  }

  getLotesByIdUser(){
     this.API.lotesByIdUser(localStorage.getItem('id_user')).subscribe(response => {
      this.lotes = response;       
      this.Tamano = this.lotes.length;
     })
  }

  eliminarLote(id , f: any){
    this.API.deleteLote(id , f).subscribe(response => {
      console.log(response);
      Swal.fire("¡Hecho!", "Lote eliminado correctamente", "info");
      this.ngOnInit();
    })
  }

  pagValid(){
    if(this.rol == '0'){
      this.route.navigate(['']);
    }
  }

  verMapa(id) {
    this.API.LoteById(id).subscribe(response => {
        if ('geolocation' in navigator) {
          navigator.geolocation.getCurrentPosition((position) => {
            this.lat = +response['lotelocalizacion'].latitud;
            this.lng = +response['lotelocalizacion'].longitud;

            this.zoom = 8;
            this.getAddress(this.lat, this.lng);
        });
      }
    }) 
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      console.log("sdsdsdds", status);
      console.log("sdfsd",results);
      if (status == 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
          Swal.fire("¡Ups!", "Ha ocurrido un error al buscar la localización", "error");
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
        Swal.fire("¡Ups!", "Ha ocurrido un error al buscar la localización", "error");
      }
 
    });
  }


  muestrasC(id){
    localStorage.setItem('id_lote', id);
    this.route.navigate(['lista-cromas']);
  }

}
