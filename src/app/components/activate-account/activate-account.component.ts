import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder , Validators } from '@angular/forms';
import { DataApiService } from '../../services/data-api.service'
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-activate-account',
  templateUrl: './activate-account.component.html',
  styleUrls: ['./activate-account.component.css']
})
export class ActivateAccountComponent implements OnInit {

  activarCuenta: FormGroup;

  constructor(private API: DataApiService, 
    public FB: FormBuilder,
    private route: Router,
    private rutaActiva: ActivatedRoute) { }

  ngOnInit() {
    this.activarCuenta = this.FB.group({
      conf_correo:['true'],
      token_activacion:['']
    })
    this.Activacion(this.activarCuenta.value)
  }

  Activacion(Conf:any){
    const token = this.rutaActiva.snapshot.params.token
    
    this.API.ConfCuenta(token, Conf).subscribe(response => {
      Swal.fire("¡Buen trabajo!", "Acabas de activar tu cuenta, ahora espera que un administrador acepte tu solicitud para poder iniciar sesión.", "success");
      this.route.navigate(['login']);
    })
  }
}
