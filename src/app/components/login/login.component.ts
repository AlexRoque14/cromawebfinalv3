import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DataApiService } from '../../services/data-api.service'
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  login: FormGroup;

  constructor(private API: DataApiService,
    public FB: FormBuilder,
    private route: Router) { }

  ngOnInit() {
    this.login = this.FB.group({
      email: [''],
      password: ['']
    });
  }

  Login(formLogin: any) {

    if (formLogin.email == "" || formLogin.password == "") {
      Swal.fire("¡Error!", "Ingrese los datos faltantes", "error");
    } else {
      this.API.login(formLogin).subscribe(response => {
        if (response) {
          if(response.user.conf_correo){
            if(response.user.activo){
              if(!response.user.eliminado){
                this.route.navigate(['analisis']);
                localStorage.setItem("rol_user", response.user.rol);
                localStorage.setItem("email", response.user.email);
                localStorage.setItem("id_user", response.user.id);
                localStorage.setItem("id_municipio", response.user.id_municipio);

                Swal.fire("¡Buen trabajo!", "Inicio de sesión exitoso", "success");
              }else{
                Swal.fire("¡Error!", "La cuenta no existe", "error");
              }
            }else{
              Swal.fire("¡Error!", "La cuenta debe ser confirmada por un administrador", "error");
            }
          }else{
            Swal.fire("¡Error!", "La cuenta no ha sido activada. Verifique su correo", "error");
          }
        }else{
          Swal.fire("¡Ups!", "Ha ocurrido un error en el inicio de sesión. Intente nuevamente", "error");
        }
      }, err => {
        Swal.fire("¡Ups!", "Usuaio o contraseña incorrectas", "error");
      })
    }

  }

}
