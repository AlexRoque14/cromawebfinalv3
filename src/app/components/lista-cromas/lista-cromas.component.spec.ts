import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaCromasComponent } from './lista-cromas.component';

describe('ListaCromasComponent', () => {
  let component: ListaCromasComponent;
  let fixture: ComponentFixture<ListaCromasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaCromasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaCromasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
