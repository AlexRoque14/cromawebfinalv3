import { Component, OnInit } from '@angular/core';
import { DataApiService } from 'src/app/services/data-api.service';
import { Router } from '@angular/router';
import { MapsAPILoader} from '@agm/core';

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})

export class MapaComponent implements OnInit {
  
  private geoCoder;
  lat: number;
  lng: number;
  zoom: number;
  viewType: any='hybrid';
  
  muestra: any;
  

  constructor(private API: DataApiService, private route: Router, private mapsAPILoader: MapsAPILoader) { }

  ngOnInit() {
    this.verMapa();
    this.muestras();
  }

  verMapa(){   
    this.lat = 16.616970100000003;
    this.lng = -93.0976835;
    this.zoom = 12;    
  }

   muestras(){
    this.API.MuestrasMapa().subscribe(response => {
      this.muestra = response
    })
   }

  selectMarker(event, Muestra:any) {
    console.log(Muestra);
  }
}



