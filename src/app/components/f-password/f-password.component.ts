import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder , Validators } from '@angular/forms';
import { DataApiService } from '../../services/data-api.service'
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-f-password',
  templateUrl: './f-password.component.html',
  styleUrls: ['./f-password.component.css']
})
export class FPasswordComponent implements OnInit {

  constructor(private API: DataApiService, 
    public FB: FormBuilder,
    private route: Router,
    private rutaActiva: ActivatedRoute) { }

  fpassword: FormGroup; 
  email: FormGroup;
  send:FormGroup;
  token = this.rutaActiva.snapshot.params.token
  ngOnInit() {
    this.inicial()
  }

  inicial(){
    
    this.email = this.FB.group({
      email:[''],
      token_activacion:['password']
    })
    this.fpassword = this.FB.group({
      password:[''],
      token_activacion:['']
    })
    this.send = this.FB.group({
      email:[''],
      token:['']
    })
  }

  SendEmail(formemail:any, sendEmail:any){
    const correo = formemail.email
    this.API.getUserEmail(correo, formemail).subscribe(response=>{
      if(response['message']){
        Swal.fire("¡Advertencia!", response['message'], "info");
      }else{
        sendEmail.email = correo
        sendEmail.token = response['token_activacion']
        
        this.API.sendEmailPassword(sendEmail).subscribe(response=>{})
        Swal.fire("¡Buen trabajo!", "Solicitud enviada. Revisa tu correo para cambiar tu contraseña.", "success");
        this.route.navigate(['login']);
      }

    })
  }

  ResetPassword(Reset:any){
    this.API.ConfCuenta(this.token, Reset).subscribe(response => {
      if(response['status'] == '404'){
        Swal.fire("¡Error!", response['message'], "error");
      }else{
        Swal.fire("¡Cambio exitoso!", "Contraseña Cambiada exitosamente.", "success");
        this.route.navigate(['login']);
      }
      
    })
  }

  back(){
    this.route.navigate(['login']);
  }
}
