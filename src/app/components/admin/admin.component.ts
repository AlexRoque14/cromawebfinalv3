import { Component, OnInit } from '@angular/core';
import { DataApiService } from 'src/app/services/data-api.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  User:any; //inactivos
  UserAc:any;
  correo : any;
  activar : FormGroup;
  desactivar : FormGroup;
  public Tamano : any; //inactivos
  public TamanoAc : any;
  public rol =  localStorage.getItem('rol_user');

  filterPost = ''

  registro: FormGroup;
  sendEmail: FormGroup;
  Paises: any;
  Estados: any;
  Municipios: any;

  Pais: String;
  Estado: String;
  Municipio: String;
  MunId: any;

  constructor(private API: DataApiService, 
    public FB: FormBuilder,
    private route: Router) { }

  async ngOnInit() {
    this.pagValid();
    this.activar = this.FB.group({
     activo: ["true"]
    });
    this.desactivar = this.FB.group({
      activo: ["false"]
    });
    this.Pais = "País"
    this.Estado = "Estado"
    this.Municipio = "Municipio"
    await this.pais()
    this.sendEmail = this.FB.group({
      email:[''],
      nombre:[''],
      token:['']
    })
    this.registro = this.FB.group({
      nombre:['', Validators.required],
      ape_p:['', Validators.required],
      ape_m:['', Validators.required],
      email:['', Validators.required],
      password:['', Validators.required],
      municipio_id:['', Validators.required],
      direccion:['', Validators.required],
      telefono:['', Validators.required],
      rol:['', Validators.required],
    })
   this.getUsersIn();
   this.getUsersAc();
  }
 

  getUsersIn(){
    this.API.getAllUsersIn().subscribe(response => {
      this.User = response['user'];
      this.Tamano = this.User.length
    })
  }

  ActivarUser(idUser:String, f:any){
    const id = idUser;
    this.API.ActivarUsuario(idUser, f).subscribe(response =>{
      alert("Usuario activado correctamente")
      this.ngOnInit();
    })
  }

  getUsersAc(){
    this.API.getAllUsersAc().subscribe(response => {
      this.UserAc = response['user'];
      this.TamanoAc = this.UserAc.length
    })
  }

  /*Actualiza al usuario*/
  DesactivarUser(idUser:String, f:any){
    console.log(idUser)
    this.API.DesactivarUsuario(idUser, f).subscribe(response =>{
      this.ngOnInit();
      alert("Usuario desactivado correctamente")
    })
  }
    
  pagValid(){
    if(this.rol != '1'){
      this.route.navigate(['']);
    }
  }
  //New User 
  register(formregistro:any, SendEmail: any){
    if (formregistro.nombre == "" || formregistro.ape_pat == ""  || formregistro.ape_mat == "" 
        || formregistro.email == "" || formregistro.password == "" || this.MunId == "" 
        || formregistro.direccion == "" || formregistro.telefono == "" || formregistro.rol == ""){
        Swal.fire("Error", "Complete los datos faltantes", "error");
    }else{
      formregistro.municipio_id = this.MunId
      this.API.registroUsuario(formregistro).subscribe(response=>{        

        SendEmail.token = response['token_activacion']
        SendEmail.email = response['email']
        SendEmail.nombre = response['nombre']

        this.ActivarUser(response['id'], this.activar.value)
        this.API.sendEmail(SendEmail).subscribe(response=>{
          Swal.fire("Usuario Registrado", "Registro Exitoso. Revisar el email del nuevo usuario para que pueda activar su cuenta.", "success");
        })
      })

    }
  }
  async pais(){
    await this.API.getPais().subscribe(async response =>{
      this.Paises = await response;
      console.log(this.Paises)
    })
  }
  extraeidPais(id, pais){
    this.Pais=pais
    this.API.getEstados(id).subscribe(response =>{
      this.Estados = response;
    })
  }
  extraeidEstado(id, estado){
    this.Estado = estado
    this.API.getMunicipios(id).subscribe(response =>{
      this.Municipios = response['estadomunicipio'];
    })
  }

  extraeidMuni(id, muni){
    this.MunId = id
    this.Municipio = muni
  }

}
