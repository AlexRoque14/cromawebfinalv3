import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private route: Router) { }
  public id = localStorage.getItem('rol_user');

  ngOnInit() {
    
  }

  logout(){
    this.route.navigate(['']);
    this.id = '0';
    localStorage.setItem("rol_user", '0');
    Swal.fire("¡Buen trabajo!", "Cierre de sesión exitoso", "success");
  }

}
