import { Component, OnInit } from '@angular/core';
import { DataApiService } from 'src/app/services/data-api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ViewChild, ElementRef, NgZone } from '@angular/core';
import { MapsAPILoader, MouseEvent } from '@agm/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registro-lote',
  templateUrl: './registro-lote.component.html',
  styleUrls: ['./registro-lote.component.css']
})
export class RegistroLoteComponent implements OnInit {

  latitude: number;
  longitude: number;
  zoom: number;
  address: string;
  private geoCoder;
  registro: FormGroup;
  viewType: any='hybrid';

  public rol =  localStorage.getItem('rol_user');

  // @ViewChild('search')
  public searchElementRef: ElementRef;

  constructor(private API: DataApiService,
    public FB: FormBuilder,
    private route: Router,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone) { }

  ngOnInit() {
    this.pagValid();
    this.registro = this.FB.group({
      user_id: [localStorage.getItem('id_user'), Validators.required],
      municipio_id: [167, Validators.required],
      longitud: ['', Validators.required],
      latitud: ['', Validators.required],
      nombre: [''],
      uso_suelo: ['', Validators.required]
    })
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;
        this.zoom = 12;
      });
  }

  registerLote(formregistro: any) {
    formregistro.latitud = this.latitude;
    formregistro.longitud = this.longitude;
    this.API.registroLote(formregistro).subscribe(response => {
      this.ngOnInit();
      Swal.fire("¡Buen trabajo!", "Registro de lote exitoso", "success");
      this.route.navigate(['lista-lotes']);
    })
  }

  pagValid(){
    if(this.rol == '0'){
      this.route.navigate(['']);
    }
  }

    // Get Current Location Coordinates
    private setCurrentLocation() {
      if ('geolocation' in navigator) {
        navigator.geolocation.getCurrentPosition((position) => {
          this.latitude = position.coords.latitude;
          this.longitude = position.coords.longitude;
          this.zoom = 8;
          this.getAddress(this.latitude, this.longitude);
        });
      }
    }
   
   
    markerDragEnd($event: MouseEvent) {
      this.latitude = $event.coords.lat;
      this.longitude = $event.coords.lng;
      this.getAddress(this.latitude, this.longitude);
    }
   
    getAddress(latitude, longitude) {
      this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            this.zoom = 12;
            this.address = results[0].formatted_address;
          } else {
            window.alert('No results found');
            Swal.fire("¡Ups!", "Ha ocurrido un error al buscar la localización", "error");
          }
        } else {
          window.alert('Geocoder failed due to: ' + status);
          Swal.fire("¡Ups!", "Ha ocurrido un error al buscar la localización", "error");
        }
   
      });
    }
}




