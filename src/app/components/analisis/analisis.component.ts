import { Component, OnInit } from '@angular/core';
import  {data_set_no_analizado} from '../../data_set/data-set-no-analizado';
import { DataApiService } from 'src/app/services/data-api.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ViewChild, ElementRef, NgZone } from '@angular/core';
import { MapsAPILoader, MouseEvent } from '@agm/core';
import Swal from 'sweetalert2'
import { async } from '@angular/core/testing';
import { resolve } from 'url';

@Component({
  selector: 'app-analisis',
  templateUrl: './analisis.component.html',
  styleUrls: ['./analisis.component.css']
})
export class AnalisisComponent implements OnInit {

  public imagePath;
  imgURL: any;
  public showAnimated=true;
  public message: string;
  public message2: string;
  public data_set:any = [];
  public recomendacion1: string;
  public recomendacion2: string;
  public rol =  localStorage.getItem('rol_user');

  public Pais = '';
  public Estado = '';
  public Municipio = '';

  public valid = true;

  public img = new FormData();
  public lotes:any;
  private ResetSave ='';
  
  public mimeType = null; 
  public lat: number;
  public lng: number;
  public latitude: number;
  public longitude: number;
  public zoom: number;
  public address: string;
  private geoCoder;
  cromann: FormGroup;
  resultado:any;
  data_setSend:FormGroup;
  idCroma : string
  viewType: any='hybrid';
  constructor(private API: DataApiService, private route: Router, public FB: FormBuilder, private mapsAPILoader: MapsAPILoader) { }

  ngOnInit() {
    this.pagValid();
    this.ResetSave = 'Guardar Información';
    this.data_set = data_set_no_analizado;
    this.getLotesByIdUser();
    this.getInfoUser();
    this.cromann = this.FB.group({
      latitud:[''],
      longitud:[''],
      lote_id:[''],
      profundidad:[''],
      user_id:[localStorage.getItem('id_user')],
      img:['']
    });

    this.mapsAPILoader.load().then(() => {
      this.geoCoder = new google.maps.Geocoder;
    });

  }

  getLotesByIdUser(){
    this.API.lotesByIdUser(localStorage.getItem('id_user')).subscribe(response => {
      this.lotes = response;
    })
  }

  getInfoUser(){
    this.API.getUserId(localStorage.getItem('id_user')).subscribe(response => {
      this.Pais = response['userPais']['nombre'];
      this.Estado = response['userEstado']['nombre'];
      this.Municipio = response['userMuni']['nombre'];
    })
  }

  preview(files) {
      if (files.length === 0)
          return;

       this.mimeType = files[0].type;
      if (this.mimeType.match(/image\/*/) == null) {
          this.message = "Only images are supported.";
          return;
      }

      var reader = new FileReader();
      this.imagePath = files[0];
      
      reader.readAsDataURL(files[0]);
      reader.onload = (_event) => {
          this.imgURL = reader.result;
      }
      this.valid = true;
      this.message2 = "Validando Croma..."
      this.message = "";
  }

  saveInformation(Croma:any){
    

    if (this.imgURL == null || Croma.lote_id == "" || Croma.profundidad == "") {
      this.message = "Complete los campos faltantes";
      this.valid = false;
      return;
    }else{
      if(this.ResetSave == 'Guardar Información'){
        setInterval(()=>{
          this.showAnimated = false;
          let close = document.querySelector('#exampleModalCenter');
        },15000);
        Croma.longitud = this.lng
        Croma.latitud = this.lat
        Croma.img = this.imagePath
  
        let IA = new FormData()
        IA.append('ourfile', Croma.img)
        this.API.IAReturn(IA).subscribe(response => {
          this.resultado = response
          for (let index = 0; index < this.resultado.length; index++) {
            if(this.resultado[index] == 1)
              this.resultado[index] = true;
            else
              this.resultado[index] = false;
          }
          
          this.data_set  = {
            "ind_bio" : this.resultado[6],
            "ind_mat_org" : this.resultado[1],
            "ind_mat_viva" : this.resultado[5],
            "ind_n_elem" : this.resultado[3],
            "ind_oxg" : this.resultado[0],
            "ind_pro_n" : this.resultado[7],
            "ind_romp" : this.resultado[4],
            "ind_trans_sist" : this.resultado[2],
          }
  
          this.data_setSend  = this.FB.group({
            ind_bio : [this.data_set['ind_bio']],
            ind_mat_org : [this.data_set['ind_mat_org']],
            ind_mat_viva : [this.data_set['ind_mat_viva']],
            ind_n_elem : [this.data_set['ind_n_elem']],
            ind_oxg : [this.data_set['ind_oxg']],
            ind_pro_n : [this.data_set['ind_pro_n']],
            ind_romp : [this.data_set['ind_romp']],
            ind_trans_sist : [this.data_set['ind_trans_sist']],
          })
  
          this.API.EditCroma(this.idCroma, this.data_setSend.value).subscribe(response =>{
            this.message = "Análisis finalizado";
            this.message2 = ""
            this.ResetSave = "Refrescar";
            console.log(response['mensaje']);
          })
  
        })
  
        let imageres = '';
        let IMG = new FormData()
        IMG.append('img', Croma.img)
        IMG.append('data', localStorage.getItem('id_user'))
  
        this.API.sendIMG(IMG).subscribe(async response => {
            imageres = response['path']
            
            let Cromanns = new FormData()
            
            Cromanns.append('longitud', Croma.longitud)
            Cromanns.append('latitud', Croma.latitud)
            Cromanns.append('lote_id', Croma.lote_id)
            Cromanns.append('profundidad', Croma.profundidad)
            Cromanns.append('user_id', Croma.user_id)
            Cromanns.append('img', imageres)
      
            this.API.addCroma(Cromanns).subscribe(response =>{
              this.idCroma = response['cromann']['id']
              
            })
        })
      }else{
        this.message2 = "Variables reiniciadas"
        this.ResetSave = 'Guardar Información';
        this.valid = false;
        this.imgURL = null
        this.message = '';
        this.data_set  = {
          "ind_bio" : false,
          "ind_mat_org" : false,
          "ind_mat_viva" : false,
          "ind_n_elem" : false,
          "ind_oxg" : false,
          "ind_pro_n" : false,
          "ind_romp" : false,
          "ind_trans_sist" : false,
        }
        this.cromann = this.FB.group({
          latitud:[''],
          longitud:[''],
          lote_id:[''],
          profundidad:[''],
          user_id:[localStorage.getItem('id_user')],
          img:['']
        });
      }
    }
   
  }

  pagValid(){
    if(this.rol == '0'){
      this.route.navigate(['']);
    }
  }

  
  verMapa(id) {
    this.API.LoteById(id).subscribe(response => {
        if ('geolocation' in navigator) {
          navigator.geolocation.getCurrentPosition((position) => {
            this.lat = +response['lotelocalizacion'].latitud;
            this.lng = +response['lotelocalizacion'].longitud;

            this.zoom = 8;
            this.getAddress(this.lat, this.lng);
        });
      }
    }) 
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
          Swal.fire("¡Ups!", "Ha ocurrido un error al buscar la localización", "error");
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
        Swal.fire("¡Ups!", "Ha ocurrido un error al buscar la localización", "error");
      }
 
    });
  }

    markerDragEnd($event: MouseEvent) {
      // console.log($event);
      this.latitude = $event.coords.lat;
      this.longitude = $event.coords.lng;
    }

    guardar(){
      this.lat = this.latitude;
      this.lng = this.longitude;
      
    }
}
