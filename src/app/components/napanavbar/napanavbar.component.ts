import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-napanavbar',
  templateUrl: './napanavbar.component.html',
  styleUrls: ['./napanavbar.component.css']
})
export class NapanavbarComponent implements OnInit {

  constructor(private route: Router) { }
  public id = localStorage.getItem('rol_user');

  ngOnInit() {
    
  }

}
