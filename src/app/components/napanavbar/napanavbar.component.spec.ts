import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NapanavbarComponent } from './napanavbar.component';

describe('NapanavbarComponent', () => {
  let component: NapanavbarComponent;
  let fixture: ComponentFixture<NapanavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NapanavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NapanavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
