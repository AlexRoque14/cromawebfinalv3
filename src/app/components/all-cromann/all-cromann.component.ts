import { Component, OnInit } from '@angular/core';
import { DataApiService } from 'src/app/services/data-api.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-all-cromann',
  templateUrl: './all-cromann.component.html',
  styleUrls: ['./all-cromann.component.css']
})
export class AllCromannComponent implements OnInit {

  muestras: any;
  // URL = 'http://192.168.43.225:3333'
  URL = 'https://cromasesiia.com'
  IMG = ''
  // public data_set = data_set_no_analizado;
  public data_setSend:FormGroup;
  public data_set:any;
  private idCroma = '0';
  public Pais = '';
  public Estado = '';
  public Municipio = '';
  public rol =  localStorage.getItem('rol_user');

  public isEdit = false;
  EdGua: String;

  constructor(private API: DataApiService,  public FB: FormBuilder, private route: Router) { }

  ngOnInit() {
    this.EdGua = "Editar"
    this.pagValid();
    this.data_set  = {
      "ind_bio" : "false",
      "ind_mat_org" : "false",
      "ind_mat_viva" : "false",
      "ind_n_elem" : "false",
      "ind_oxg" : "false",
      "ind_pro_n" : "false",
      "ind_romp" : "false",
      "ind_trans_sist" : "false",
    }
    this.getCromas();
    this.getInfoUser();
  }

  pagValid(){
    if(this.rol != '1' && this.rol != '2'){
      this.route.navigate(['']);
    }
  }

  getCromas(){
    this.API.AllCromas().subscribe(response=>{
      this.muestras = response
      console.log(this.muestras);
      
    });
  }

  getInfoUser(){
    this.API.getUserId(localStorage.getItem('id_user')).subscribe(response => {
      this.Pais = response['userPais']['nombre'];
      this.Estado = response['userEstado']['nombre'];
      this.Municipio = response['userMuni']['nombre'];
    })
  }

  editar(){
    if(this.isEdit){
      this.isEdit = false;
      this.EdGua = "Editar"
      this.data_setSend  = this.FB.group({
        ind_bio : [this.data_set['ind_bio']],
        ind_mat_org : [this.data_set['ind_mat_org']],
        ind_mat_viva : [this.data_set['ind_mat_viva']],
        ind_n_elem : [this.data_set['ind_n_elem']],
        ind_oxg : [this.data_set['ind_oxg']],
        ind_pro_n : [this.data_set['ind_pro_n']],
        ind_romp : [this.data_set['ind_romp']],
        ind_trans_sist : [this.data_set['ind_trans_sist']],
        veri : ['true'],
      })
      this.API.EditCroma(this.idCroma, this.data_setSend.value).subscribe(response =>{
        Swal.fire("Editado", response['mensaje'], "success");
      })
    }else{
      this.isEdit = true;
      this.EdGua = "Guardar"
      Swal.fire("Editar", 
      "Ahora puede editar presionando los botones negros o verdes \n(si el botón es negro es porque no tiene presencia pero si es de color verde es porque si tiene presencia de mineral).\n\nCuando termine de editar por favor presione el botón 'Guardar'",
        "info");
    }
    
  }
  close(){
    if(this.isEdit){
      this.isEdit = false;
    }
  }

  Muestra(id:string){
    this.idCroma = id;
    this.API.CromaById(id).subscribe(response =>{
      // console.log(response['cromann']['img']);
      this.IMG = response['cromann']['img']
      this.data_setSend  = this.FB.group({
        ind_bio : [response['cromann']['ind_bio']],
        ind_mat_org : [response['cromann']['ind_mat_org']],
        ind_mat_viva : [response['cromann']['ind_mat_viva']],
        ind_n_elem : [response['cromann']['ind_n_elem']],
        ind_oxg : [response['cromann']['ind_oxg']],
        ind_pro_n : [response['cromann']['ind_pro_n']],
        ind_romp : [response['cromann']['ind_romp']],
        ind_trans_sist : [response['cromann']['ind_trans_sist']],
        veri : [response['cromann']['veri']],
      })
      this.data_set  = {
        "ind_bio" : response['cromann']['ind_bio'],
        "ind_mat_org" : response['cromann']['ind_mat_org'],
        "ind_mat_viva" : response['cromann']['ind_mat_viva'],
        "ind_n_elem" : response['cromann']['ind_n_elem'],
        "ind_oxg" : response['cromann']['ind_oxg'],
        "ind_pro_n" : response['cromann']['ind_pro_n'],
        "ind_romp" : response['cromann']['ind_romp'],
        "ind_trans_sist" : response['cromann']['ind_trans_sist'],
      }
    })
  }

  EditarIndOx(Valor:boolean){
    if(Valor){
      this.data_set["ind_oxg"] = false
    }else{
      this.data_set["ind_oxg"] = true
    }
  }

  EditarIndMO(Valor:boolean){
    if(Valor){
      this.data_set["ind_mat_org"] = false
    }else{
      this.data_set["ind_mat_org"] = true
    }
  }

  EditarIndN(Valor:boolean){
    if(Valor){
      this.data_set["ind_n_elem"] = false
    }else{
      this.data_set["ind_n_elem"] = true
    }
  }

  EditarIndR(Valor:boolean){
    if(Valor){
      this.data_set["ind_romp"] = false
    }else{
      this.data_set["ind_romp"] = true
    }
  }

  EditarIndMV(Valor:boolean){
    if(Valor){
      this.data_set["ind_mat_viva"] = false
    }else{
      this.data_set["ind_mat_viva"] = true
    }
  }

  EditarIndP(Valor:boolean){
    if(Valor){
      this.data_set["ind_pro_n"] = false
    }else{
      this.data_set["ind_pro_n"] = true
    }
  }

  EditarIndTS(Valor:boolean){
    if(Valor){
      this.data_set["ind_trans_sist"] = false
    }else{
      this.data_set["ind_trans_sist"] = true
    }
  }

  EditarIndAB(Valor:boolean){
    if(Valor){
      this.data_set["ind_bio"] = false
    }else{
      this.data_set["ind_bio"] = true
    }
  }

}
