import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllCromannComponent } from './all-cromann.component';

describe('AllCromannComponent', () => {
  let component: AllCromannComponent;
  let fixture: ComponentFixture<AllCromannComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllCromannComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllCromannComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
